#include "../TrigInDetTrackFitter.h"
#include "../TrigDkfTrackMakerTool.h"
#include "../TrigInDetTrackFollowingTool.h"

DECLARE_COMPONENT( TrigInDetTrackFitter )
DECLARE_COMPONENT( TrigDkfTrackMakerTool )
DECLARE_COMPONENT( TrigInDetTrackFollowingTool )
